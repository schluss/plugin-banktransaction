const pluginJson = require('./plugin.json');

const rabobank = {
    /**
     *  Reading file. 
     *      Params:
     *          - filelocation: File location. 
     */
    readFile: (filelocation) => {
        return new Promise((resolve, reject) => {
            let pluginList = pluginJson.plugins;    // Getting plug-ins from plugin.json and setting Plug-in list.
            let extension = getExtension(filelocation);     // Getting and setting extension.
        
            if (pluginList[extension] != undefined) {   // Registered provider found:
                let pluginSrc=pluginList[extension].src;    // Setting Plug-in source.

                import (/* webpackMode: "eager" */ `./src/${pluginSrc}`).then((x) => {
                    x.readFile(filelocation).then((e) => {
                        console.log("Config Response" + e);

                        resolve(e);
                    }).catch((error) => {
                        reject(error);
                    });
                }).catch((error) => {
                    reject(error);
                });
            } else {    // Registered provider not found:
                reject("NO extension Reader Found");
            }
        });
    },
};

/**
 *  Getting Extension.
 *      Params:
 *          - file: File location.
 *      Returns:
 *          - extension.
 */
function getExtension(file) {
    extension = file.split('.').pop();
    
    return extension;
}

module.exports = rabobank;