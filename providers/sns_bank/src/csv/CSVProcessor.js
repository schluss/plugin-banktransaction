const json = require('./template.json');
let attributeList = json.attributes;   

/**
 * Reading CSV File Content Processor.
 * 
 */
const CSVProcessor = {
    readFile: (fileLocation) => {
        return new Promise((resolve, reject) => {
            window.resolveLocalFileSystemURL(fileLocation, resolve, reject);
        }).then((fileEntry) => {
            return getContentFromFile(fileEntry);
        }).then((data) => {
            return getTemplateData(data);
        });
    }
};

function reject(e) {
	alert("FileSystem Error : "  + e.code);
	console.dir(e);
}

/**
 * Read CSV Content
 * @param {string} fileEntry 
 * @returns {string} csvContent
 */
function getContentFromFile(fileEntry) {
    return new Promise((resolve, reject) => {
        fileEntry.file((file) => {
            let reader = new FileReader();

            reader.onloadend = () => {
                resolve(reader.result )
            };

            reader.onerror = () => {
                reject(reader.error )
            };
            
            reader.readAsText(file);
        }, reject);
    });
}

/**
 * Process Content with Template
 * @param {String} result 
 */
function getTemplateData(csvData){
    return new Promise((resolve, reject) => {
        let sanitisedData;
        let headings = [];
        let data = [];
        let attibute;
        let value;
        let response = [];
        let returnList = {};

        sanitisedData = csvData.split(/(\r\n)/);

        console.log("sanitisedData : " + sanitisedData);
        
        for (let i = 0; i < sanitisedData.length; i++) {
            if (i == 0) {
                headings = sanitisedData[i].split(',');

                console.log("Headings: \n\r" + headings);
            } else if (i % 2 == 0) {
                data = sanitisedData[i].split(',');

                console.log("Data: \n\r" + data);
                let responseField = [];

                for (let j = 0; j < attributeList.length; j++) {
                    if (attributeList[j].visible == "true") {
                        attibute = attributeList[j].attibute_name;
                        value = data[j];
                        responseField.push({"name":attibute ,"value": value});
                    }
                }
                response.push(responseField);     
            }
        }
        returnList["payload"] = response;
        resolve(JSON.stringify(returnList));
    });
}

module.exports = CSVProcessor;