	<style>
		.panelcontainer {
			--n: 1;
			display: flex;
			align-items:center;
			overflow-y: hidden;
			width: 100%;
			width: calc(var(--n)*100%);
			
			/*height: 50vw;
			max-height: 100vh;	*/
			
			transform: translate(calc(var(--tx, 0px) + var(--i, 0)/var(--n)*-100%));
		}

		.panel {
			width: 100%;
			width: calc(100%/var(--n));
			user-select: none;
			pointer-events: none;
			cursor:pointer;
			padding-left:2em;
			padding-right:2em;
		}
		
		.panel img {
			width:100%;
			max-width:150px;
		}

		.smooth {
			transition: transform calc(var(--f, 1)*.5s) ease-out;
		}
		
		.progress {
			display:inline-block;
			width:8px;
			height:8px;
			background:#e2e1e9;
			border-radius:100%;
			margin-right:7px;
			
			transition: background-color 0.2s linear;
		}
		
		.progress.on {
			background:#586071;
		}

		#processingWindow {
			position: absolute;
			width: 200px;
			height: 200px;
			left: 50%;
			margin-top: 68%;
			transform: translate(-50%, -50%);
		}

		h6 {
			text-align: center;
		}

		.ani-img {
			width:100px;
			display: block;
			margin-left: auto;
			margin-right: auto; 
			width: 70%;
		}
	</style>	

<div id="contentWindow">
	<header>
		<div class="frame">
			<div class="logo" style="float:left;">
				<img src="assets/img/logo-pink.svg" alt="logo" width="49" />
			</div>
			
			<nav id="mainnav">
				<div id="slider">
					<div class="logo">
						<a href="#/"><img src="assets/img/logo-white.svg" alt="logo" width="49" /></a>
					</div>			
				
					<ul class="">
						<li><a href="#/">Dashboard</a></li>
						<li><a href="#/data">Gegevens</a></li>
						<li><a href="#/shares">Inzages</a></li>
					</ul>
					
					<ul class="smaller">
						<li><a href="https://www.schluss.org" target="_blank"><img src="assets/img/support.svg" alt="logo" width="20"/> Support</a></li>
						<li><a href="#/logout"><img src="assets/img/logout.svg" alt="logout" width="20" /> Uitloggen</a></li>
					</ul>
				</div>
				
				<div id="hamburger">
					<span></span>
					<span></span>
					<span></span>		
				</div>
			</nav>
		</div>
	</header>

	<main>
		<div id="processingWindow" class="frame">
			<h2>Verwerken...</h2>
			
			<h6>Je gegevens</h6>
			<h6>worden verwerkt</h6>
			<h6>en opgeslagen...</h6><br/>
			
			<img class="ani-img" src="assets/img/processing.gif" alt="processing">
		</div>
	</main>

	<footer>	
	</footer>
</div>

<script>
	$('#hamburger').click(function(){
		$('#mainnav').toggleClass('show');
	});
	
	$('#idinbtn').click(function(){
		window.location.href='#/idin/about';
	});
</script>