/**
 *  Functionality of all Bank Transactions.
 * 
 */
const providersJson = require('./providers.json');
const util = require('./util');

const config = {
    viewpath: './app_modules/node_modules/@schluss/plugin-banktransactions/',
};

module.exports = {
    init: (context) => {
        if (! context.request){
            console.log("generate sample request object");
            
            context.request = {
                id: '018eedd1-d21b-448b-91f0-da5db6a03006', // this is the requestID (format UUIDv4).
                name: 'de Volksbank',
                pubkey: '-----BEGIN PGP PUBLIC KEY BLOCK----- ... -----END PGP PUBLIC KEY BLOCK-----', // the users public key.
                settingsuri: 'https://url-to-volksbank.config.json',    // if needed, the plugin can find additional details about what is requested by downloading the config.json combined with the 'scope' parameter.
                scope: 'hypotheekaanvraag'
            };

        }
        
        let ui = context.sammy;
        
        ui.partial(config.viewpath + 'views/banklist.tpl', null, () => {
            let providersList = providersJson.providers;
            let bankList = document.getElementById("listofbanks");

            for (let i = 0; i < Object.keys(providersList).length; i++) {
                const div = document.createElement('div');
                div.className = 'row';
                    var str="";
                    str+='<ul class="list-group list-group-flush full_div">';
                    str+='<li class="list-group-item"><img src="./app_modules/node_modules/@schluss/plugin-banktransactions/views/assets/img/bank1.png" alt="abort" style="vertical-align:middle; height: 30px;width: 30px;" />';
                    str+='&nbsp'+ Object.values(providersList)[i].name;
                    str+=' <div class="round">';
                    str+=' <input type="radio" name="a" value="'+Object.keys(providersList)[i]+'" id="'+Object.keys(providersList)[i]+'" />';
                    str+='  <label for="'+Object.keys(providersList)[i]+'"></label>';
                    str+='  </div>';
                    str+='  </li>';
                    str+='  </ul>';
                div.innerHTML=str;
                bankList.appendChild(div);
            }
           
            
            //Trigger company select event:
            processBtn.onclick=function(){
                var selectedbankId;
                $('input[type="radio"]:checked').each(function() {
                    console.log(this.value);
                    selectedbankId=this.value;
                }); 
                let target = "_blank";
                let options = "location=no,hidden=yes,zoom=no";
                let url = providersList[selectedbankId].loginUrl;   
                 // import related plugin json file from selected bank folder location:
                 import(/* webpackMode: "eager" */`./providers/${selectedbankId}/plugin.json`).then((json) => {
                        console.log(JSON.stringify(json));
                        let pluginList = json.plugins;
                    let expectFileTypes = Object.keys(pluginList); // Registered expected file types
                    
                    if (expectFileTypes !== undefined) {
                        //Start download process
                        util.downloadUrl(url, target, options, expectFileTypes).then((downloadUrl) => {
                            ui.partial('./views/retrieve_transactions.tpl',null, () => {}).then((e) => {
                                document.getElementById('download_processing').style.display = 'inline';  
                                document.getElementById('download_done').style.display = 'none';  
                            });                             
                            let fileName = downloadUrl.split('/').pop();
                            return util.download(downloadUrl, cordova.file.dataDirectory+fileName);
                        }).then((downloadedLocation) => {    //when download process is finished:
                            ui.partial('./views/retrieve_transactions.tpl',null, () => {}).then((e) => {
                                document.getElementById('download_processing').style.display = 'none';  
                                document.getElementById('download_done').style.display = 'inline';  
                                util.processFile(selectedbankId, downloadedLocation).then((arr) => {    // pass downloaded file in to the parser module   
                                    console.log('No of transactions: ', arr.length);     
                                    context.store(arr, context.request.id, 'plugin-banktransactions').then(() =>{
                                        context.redirect(context.request.id);
                                    });
                                    //context.bridge.redirect(context.request.id);
                                }).catch((error) => {
                                    console.log(error.message);
                                });
                            }); 
                        });
                    } else {
                        console.log(expectFileTypes);
                        
                        alert("Unsuported file format");
                    }
                });  
            }
        });
    },
}