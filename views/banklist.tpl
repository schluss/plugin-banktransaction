<link href="./app_modules/node_modules/@schluss/plugin-banktransactions/views/assets/css/banklist.css" rel="stylesheet" type="text/css" />

<script>
	document.querySelector('body').classList.remove('grey');
</script>

<div class="container">
	<div class="row header">
		<div class="col-3">
			<a class="text-muted align-left" href="">
				<img src="assets/img/arroyhead_left.png" style="vertical-align: -1px; height: 11px;" alt=""/>&nbsp; Terug
			</a>
		</div>
		
		<div class="col-6 centered">
			<h6 style="margin-top: 3px; font-weight: bold; color: #313031;">Kies je bank</h6>
		</div>
		
		<div class="col-3">
			<a class="" href="">
				<img src="assets/img/btn-colse_pink.png" class="closeBtn" alt=""/>
			</a>
		</div>
	</div>

	<div id="listofbanks"></div>

	<div class="row footer fixed-bottom" style="position: fixed;">
		<div class="col centered">
			<button type="button" name="processBtn" id="processBtn" class="longpress">Ga verder</button>
			<br/><br/>
			<p class="light" id="longpressMsg" style="display:none;">Houd de knop 3 seconden vast</p>
		</div>
	</div>
</div>