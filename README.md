# Plugin integration test

## Install all node_modules
> ./app/npm install

## Install all app_modules (plugins)
This needs to be done per plugin:
> ./app/npm install --prefix ./app_modules ../npmjs.org/banktransactions

## Build the code
> npm run build

## Run demo:
Fires up a local https server
> npm run start


# Additional notes:

- All plugins are located in the npmjs.org/[pluginname] folder for now (later on of course online)
- The main file at every plugin needs to be in src/plugin.js
- Every plugin that has static assets (tpl/img/css etc) needs them placed in the [pluginname]/assets folder. Only then they will be copied* to the dist folder
- There

** still a bug in webpack.config.js: copying the plugin's static assets is not working. It copies all files 

