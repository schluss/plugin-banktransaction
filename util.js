module.exports = {
    /**
     *  Getting Download file URL 
     *      Params:
     *          - url: URL need to be opened
     *          - target: target type ( _blank / _self / _system )
     *          - Options: addional options provided in Inappbrowser
     *          - expectFileTypes: Expected File types/Extension array 
	 */
	downloadUrl: (url, target, options, expectFileTypes) => {
		return new Promise((resolve, reject) => {
			let ref = window.open(url, target, options);
	
			ref.addEventListener('loadstart', (event) =>{ 
				console.log("InappBrowser load start event triggered" );
				
				ref.show();
			});
        
            ref.addEventListener('beforeload', (event) => {
				console.log("InappBrowser beforeload event triggered " + event.url);

				let currentUrl = event.url;
				let extension = currentUrl.split('.').pop();
				
				if (expectFileTypes.includes(extension)) {
					console.log("Going back to Schluss App.");
					
					document.getElementById("banklistWindow").className = "hide";
					//document.getElementById("downloadWindow").className = "show";
					
					ref.close();
					resolve(currentUrl);
				}
			});

			ref.addEventListener('download', (event) => {
				console.log("InappBrowser download event triggered " + event.fileName);

				// let currentUrl = event.url;
				// let extension = currentUrl.split('.').pop();
				
				// if (expectFileTypes.includes(extension)) {
				// 	console.log("Going back to Schluss App.");
					
				// 	document.getElementById("downloadWindow").className = "show";
					
				// 	ref.close();
				document.getElementById("banklistWindow").className = "hide";
				//document.getElementById("downloadWindow").className = "show";
				ref.close();
				resolve(event.url);
				// }
			});
	
            ref.addEventListener('loadstop', (event) => {
				console.log("InappBrowser loadstop event triggered " + event.url);

				let currentUrl = event.url;
				let extension = currentUrl.split('.').pop();
				
				if (expectFileTypes.includes(extension)) {
					console.log("Going back to Schluss App.");

					document.getElementById("banklistWindow").className = "hide";
					//document.getElementById("downloadWindow").className = "show";
					
					ref.close();
					resolve(currentUrl);
				}
			});
		
			ref.addEventListener('loaderror', (event) => {
				console.log("InappBrowser load stop Error triggered" + event.message + event.code);
				
				//alert('Error is occured while loading the page.');

				document.getElementById("banklistWindow").className = "show";
				//document.getElementById("downloadWindow").className = "hide";
				
				//ref.close();
			});
		
			ref.addEventListener('exit', (event) => {
				console.log("InappBrowser exit event triggered");
				
				ref.close();
				ref = undefined;
			});
		});
	},

    /**
     *  Download files 
     *      Params:
     *          - downloadUrl: URL need to be Download
     *          - downloadLocation: Download location
     *      return 
     *          - Downloaded Location
     */
	download: (downloadUrl, downloadLocation) => {
		return new Promise((resolve, reject) => {
			console.log("Download is started!");

			if(downloadUrl.startsWith("file:")){
				alert("already Downloaded " + downloadUrl);
				resolve(downloadUrl);
			}else{
				console.log("downloadUrl.match(http)" + downloadUrl.match("http"));
				let fileTransfer = new FileTransfer();
			
				fileTransfer.download(
					downloadUrl,
					downloadLocation,
					(entry) => {
						console.log("Download complete: " + entry.toURL());
	
						resolve(entry.toURL());
					},
					(error) => {
						console.log("Download error source: " + error.source);
						console.log("Download error target: " + error.target);
						console.log("Download error code: " + error.code);
	
						alert("Download error is occured: " + error.source);
					},
					false
				);
			}
		});
    },
    
    
    /**
     *  Processing file
     *      Params:
     *          - providerId : URL need to be Download
     *          - downloadedFileLocation : Downloaded file location
     *      return 
     *          - transaction info.
     */
    processFile: (providerId, downloadedFileLocation) => {
        return new Promise((resolve, reject) => {
            import(/* webpackMode: "eager" */`./providers.json`).then((json) => {
				let providersList = json.providers;
				console.log('provider list');
				console.log(JSON.stringify(providersList));
                let provider = providersList[providerId].providerId;

                if (provider === providerId) {
                    import(/* webpackMode: "eager" */`./providers/${providerId}/config`).then((config) => {
                        config.readFile(downloadedFileLocation).then((e) => {
							console.log("Transaction Factory " + e);
							
                            resolve(e);
                        }).catch((error) => {
							console.log(error.message);
							
                            reject(error.message);
                        });
                    }).catch((error) => {
						console.log(error);
						
                        reject(error.message);
                    });
                } else {
                    //Todo registered provider not found
                    reject("Providers Not found :" + providerId);
                }
            }).catch((error) => {
				console.log(error);
				
                reject(error.message);
            });
        });    
    }
}